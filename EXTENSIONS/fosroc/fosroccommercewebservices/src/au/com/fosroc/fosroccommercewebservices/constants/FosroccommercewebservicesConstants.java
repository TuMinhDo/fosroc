/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.fosroccommercewebservices.constants;

/**
 * Global class for all Fosroccommercewebservices constants. You can add global constants for your extension into this class.
 */
public final class FosroccommercewebservicesConstants extends GeneratedFosroccommercewebservicesConstants
{
	public static final String EXTENSIONNAME = "fosroccommercewebservices";

	private FosroccommercewebservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "fosroccommercewebservicesPlatformLogo";
}
