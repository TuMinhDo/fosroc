/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.fosroccommercewebservices.service.impl;

import de.hybris.platform.commercewebservicescommons.annotation.SiteChannelRestrictionAspect;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;
import org.aspectj.lang.ProceedingJoinPoint;


/**
 * Aspect implementation for restricting an annotated OCC endpoint based on site channel
 */
public class DefaultFosrocSiteChannelRestrictionAspect extends SiteChannelRestrictionAspect {

	public DefaultFosrocSiteChannelRestrictionAspect(BaseSiteService baseSiteService, ConfigurationService configurationService) {
		super(baseSiteService, configurationService);
	}

	@Override
	public Object validateSiteChannel(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable
	{
//		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();
//		final Method method = getMethod(proceedingJoinPoint);
//
//		if (!isBaseSiteAllowed(currentBaseSite, method))
//		{
//			throw new AccessDeniedException("It's not allowed to execute this call from the current channel");
//		}

		return proceedingJoinPoint.proceed();
	}

}
