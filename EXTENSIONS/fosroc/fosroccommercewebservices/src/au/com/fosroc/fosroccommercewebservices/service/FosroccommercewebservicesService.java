/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.fosroccommercewebservices.service;

public interface FosroccommercewebservicesService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
