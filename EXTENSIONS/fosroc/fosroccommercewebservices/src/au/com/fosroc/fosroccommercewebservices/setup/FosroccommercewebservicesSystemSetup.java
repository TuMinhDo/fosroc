/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.fosroccommercewebservices.setup;

import static au.com.fosroc.fosroccommercewebservices.constants.FosroccommercewebservicesConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import au.com.fosroc.fosroccommercewebservices.constants.FosroccommercewebservicesConstants;
import au.com.fosroc.fosroccommercewebservices.service.FosroccommercewebservicesService;


@SystemSetup(extension = FosroccommercewebservicesConstants.EXTENSIONNAME)
public class FosroccommercewebservicesSystemSetup
{
	private final FosroccommercewebservicesService fosroccommercewebservicesService;

	public FosroccommercewebservicesSystemSetup(final FosroccommercewebservicesService fosroccommercewebservicesService)
	{
		this.fosroccommercewebservicesService = fosroccommercewebservicesService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		fosroccommercewebservicesService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return FosroccommercewebservicesSystemSetup.class.getResourceAsStream("/fosroccommercewebservices/sap-hybris-platform.png");
	}
}
