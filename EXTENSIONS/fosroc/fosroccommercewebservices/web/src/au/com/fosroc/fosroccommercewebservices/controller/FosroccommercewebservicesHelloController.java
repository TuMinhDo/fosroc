/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.fosroccommercewebservices.controller;

import static au.com.fosroc.fosroccommercewebservices.constants.FosroccommercewebservicesConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.fosroc.fosroccommercewebservices.service.FosroccommercewebservicesService;


@Controller
public class FosroccommercewebservicesHelloController
{
	@Autowired
	private FosroccommercewebservicesService fosroccommercewebservicesService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(final ModelMap model)
	{
		model.addAttribute("logoUrl", fosroccommercewebservicesService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
		return "welcome";
	}
}
