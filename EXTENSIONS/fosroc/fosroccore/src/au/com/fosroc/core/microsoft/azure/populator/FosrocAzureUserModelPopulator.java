/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.azure.populator;

import au.com.fosroc.core.constants.FosrocCoreConstants;
import au.com.fosroc.core.microsoft.azure.model.FosrocAzureUserModel;
import au.com.fosroc.core.microsoft.azure.util.FosrocAzureDefaultPasswordProvider;
import com.microsoft.graph.models.extensions.ObjectIdentity;
import com.microsoft.graph.models.extensions.PasswordProfile;
import de.hybris.platform.b2b.model.B2BCustomerModel;

import java.util.LinkedList;
import java.util.List;

/**
 * Populate SAP Commerce customer model to Azure AD B2C user account model
 */

public class FosrocAzureUserModelPopulator {

    public static void populate(B2BCustomerModel source, FosrocAzureUserModel target) {

        // user display name
        target.displayName = source.getDisplayName();

        // user identities
        List<ObjectIdentity> userIdentities = new LinkedList<ObjectIdentity>();
        ObjectIdentity identity = new ObjectIdentity();
        identity.signInType = FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_SIGN_IN_TYPE;
        identity.issuer = FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_TENANT_ID;
        identity.issuerAssignedId = source.getUid();
        userIdentities.add(identity);
        target.identities = userIdentities;

        // user password profile
        PasswordProfile passwordProfile = new PasswordProfile();
        passwordProfile.password = FosrocAzureDefaultPasswordProvider.generateInitialPassword(source);
        passwordProfile.forceChangePasswordNextSignIn = Boolean.FALSE;
        target.passwordProfile = passwordProfile;

        // user password policies
        target.passwordPolicies = FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_PASSWORD_POLICY;
    }
}
