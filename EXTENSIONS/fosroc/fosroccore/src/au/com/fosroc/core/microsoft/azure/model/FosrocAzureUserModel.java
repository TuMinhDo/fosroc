/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.azure.model;

import com.microsoft.graph.models.extensions.User;

import java.io.Serializable;

/**
 * Serialize this object's instance to be published by event service
 */

public class FosrocAzureUserModel extends User implements Serializable {

    public FosrocAzureUserModel() {}
}
