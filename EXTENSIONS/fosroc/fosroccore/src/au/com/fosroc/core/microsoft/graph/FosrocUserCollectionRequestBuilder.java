/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.graph;

import com.microsoft.graph.core.IBaseClient;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.extensions.IUserCollectionRequest;
import com.microsoft.graph.requests.extensions.UserCollectionRequestBuilder;

import java.util.List;

/**
 * Construct a HTTP Request send to Microsoft Graph API Endpoint
 */

public class FosrocUserCollectionRequestBuilder extends UserCollectionRequestBuilder {

    public FosrocUserCollectionRequestBuilder(String requestUrl, IBaseClient client,
                                              List<? extends Option> requestOptions) {
        super(requestUrl, client, requestOptions);
    }

    @Override
    public IUserCollectionRequest buildRequest(List<? extends Option> requestOptions) {

        return new FosrocUserCollectionRequest(this.getRequestUrl(), this.getClient(), requestOptions);
    }
}
