/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.core.constants;

/**
 * Global class for all FosrocCore constants. You can add global constants for your extension into this class.
 */
public final class FosrocCoreConstants extends GeneratedFosrocCoreConstants
{
	public static final String EXTENSIONNAME = "fosroccore";


	private FosrocCoreConstants()
	{
		//empty
	}

	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";

	// Microsoft Azure AD B2B - FOSROC Account Management Application (Client) Registration

	public static final String FOSROC_ACCOUNT_MANAGEMENT_TENANT_ID = "digitaldevb2c.onmicrosoft.com";
	public static final String FOSROC_ACCOUNT_MANAGEMENT_TENANT_GUID = "digitaldevb2c.onmicrosoft.com";

	public static final String FOSROC_ACCOUNT_MANAGEMENT_CLIENT_ID = "0cb78ec3-484f-4202-bba8-8c6b746663fc";
	public static final String FOSROC_ACCOUNT_MANAGEMENT_CLIENT_SECRET = "Mca51K.-V5Mu8.Lw883pu~05.~U0eHf8X8";
	public static final String FOSROC_ACCOUNT_MANAGEMENT_SCOPE = "https://graph.microsoft.com/.default";

	public static final String FOSROC_ACCOUNT_MANAGEMENT_SIGN_IN_TYPE = "emailAddress";
	public static final String FOSROC_ACCOUNT_MANAGEMENT_PASSWORD_POLICY = "DisablePasswordExpiration";

}
