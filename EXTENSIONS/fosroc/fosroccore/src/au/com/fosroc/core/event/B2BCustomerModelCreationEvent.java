/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.event;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.PublishEventContext;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;

/**
 * {@link B2BCustomerModelCreationEvent}
 */

public class B2BCustomerModelCreationEvent extends AbstractEvent implements ClusterAwareEvent {

    private Object source;

    public B2BCustomerModelCreationEvent(Serializable source) {

        super(source);
        this.source = source;
    }

    @Override
    public boolean canPublish(PublishEventContext publishEventContext) {
        return this.canPublish(publishEventContext);
    }
}
