/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.azure.adb2c.impl;

import au.com.fosroc.core.constants.FosrocCoreConstants;
import au.com.fosroc.core.microsoft.azure.adb2c.B2BCustomerADB2CUserReplicationService;
import au.com.fosroc.core.microsoft.graph.FosrocGraphServiceClient;
import com.microsoft.graph.auth.confidentialClient.ClientCredentialProvider;
import com.microsoft.graph.auth.enums.NationalCloud;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.requests.extensions.IUserCollectionRequest;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

/**
 * SAP Commerce service implementation used to replicate B2B customer model to Microsoft AD B2C user account identity
 */

public class DefaultB2BCustomerADB2CUserReplicationService implements B2BCustomerADB2CUserReplicationService {

    private static final Logger LOG = Logger.getLogger(DefaultB2BCustomerADB2CUserReplicationService.class);

    @Override
    public boolean replicate(User user) {

        List<String> scopes = Collections.singletonList(FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_SCOPE);

        ClientCredentialProvider clientCredentialAuthenticationProvider = new ClientCredentialProvider(
                FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_CLIENT_ID, scopes,
                FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_CLIENT_SECRET,
                FosrocCoreConstants.FOSROC_ACCOUNT_MANAGEMENT_TENANT_GUID, NationalCloud.Global);

        IGraphServiceClient graphServiceClient = FosrocGraphServiceClient.instance().
                authenticationProvider(clientCredentialAuthenticationProvider).buildMSGraphClient();

        IUserCollectionRequest userCollectionRequest = graphServiceClient.users().buildRequest();

        if (null == userCollectionRequest.post(user)) {
            return false;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("User account replicated successfully to Azure AD B2C tenant");
        }

        return true;
    }
}
