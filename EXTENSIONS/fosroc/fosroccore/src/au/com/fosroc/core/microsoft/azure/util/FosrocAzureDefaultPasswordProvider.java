/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.azure.util;

import de.hybris.platform.b2b.model.B2BCustomerModel;

/**
 * Provide Azure AD B2C default user's password
 */

public class FosrocAzureDefaultPasswordProvider {

    public static String generateInitialPassword(B2BCustomerModel customer) {
        return customer.getUid();
    }
}
