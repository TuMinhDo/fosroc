/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.graph;

import com.microsoft.graph.core.IBaseClient;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.extensions.UserCollectionRequest;

import java.util.List;

/**
 * Present a HTTP Request send to Microsoft Graph API Endpoint
 */

public class FosrocUserCollectionRequest extends UserCollectionRequest {

    private static final String CONTENT_TYPE        = "Content-Type";
    private static final String CONTENT_TYPE_VALUE  = "application/json";
    private static final String ACCEPT              = "Accept";
    private static final String ACCEPT_VALUE        = "application/json";

    public FosrocUserCollectionRequest(String requestUrl, IBaseClient client, List<? extends Option> requestOptions) {

        super(requestUrl, client, requestOptions);

        this.addHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        this.addHeader(ACCEPT, ACCEPT_VALUE);

        // debug http message header parameters (key-value)
        for (HeaderOption header : this.getHeaders()) {
            System.out.println("DEBUG: " + header.getName() + "=" + header.getValue().toString());
        }
    }
}
