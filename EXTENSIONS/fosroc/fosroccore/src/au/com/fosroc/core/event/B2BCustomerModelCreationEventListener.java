/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.event;

import au.com.fosroc.core.microsoft.azure.adb2c.B2BCustomerADB2CUserReplicationService;
import au.com.fosroc.core.microsoft.azure.model.FosrocAzureUserModel;
import com.microsoft.graph.models.extensions.User;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

/**
 * {@link B2BCustomerModelCreationEventListener}
 */

public class B2BCustomerModelCreationEventListener extends AbstractEventListener<B2BCustomerModelCreationEvent> {

    private B2BCustomerADB2CUserReplicationService replicationService;

    @Override
    protected void onEvent(B2BCustomerModelCreationEvent event) {

        // replicate Azure AD user using replication service
        if (event.getSource() instanceof FosrocAzureUserModel) {

            User user = (User) event.getSource();
            replicationService.replicate(user);
        }
    }

    public void setReplicationService(B2BCustomerADB2CUserReplicationService replicationService) {
        this.replicationService = replicationService;
    }

}
