/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.azure.adb2c;

import com.microsoft.graph.models.extensions.User;

/**
 * SAP Commerce service used to replicate B2B customer model to Microsoft AD B2C user account identity
 */

public interface B2BCustomerADB2CUserReplicationService {

    boolean replicate(final User user);
}
