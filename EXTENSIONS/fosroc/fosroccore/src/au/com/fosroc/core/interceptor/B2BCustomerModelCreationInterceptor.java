/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.interceptor;

import au.com.fosroc.core.event.B2BCustomerModelCreationEventListener;
import au.com.fosroc.core.microsoft.azure.adb2c.B2BCustomerADB2CUserReplicationService;
import au.com.fosroc.core.microsoft.azure.model.FosrocAzureUserModel;
import au.com.fosroc.core.microsoft.azure.populator.FosrocAzureUserModelPopulator;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This interceptor ensures that all new B2B Customers are successfully validated
 * and then replicated to Azure AD B2C tenant {@link B2BCustomerModel}
 */

public class B2BCustomerModelCreationInterceptor implements ValidateInterceptor<B2BCustomerModel> {

    @Autowired
    private EventService eventService;

    @Autowired
    private B2BCustomerADB2CUserReplicationService replicationService;

    @Override
    public void onValidate(B2BCustomerModel model, InterceptorContext interceptorContext) throws InterceptorException {

        // register B2B customer creation event listener
        eventService.registerEventListener(new B2BCustomerModelCreationEventListener());

        FosrocAzureUserModel user = new FosrocAzureUserModel();
        FosrocAzureUserModelPopulator.populate(model, user);

        //B2BCustomerModelCreationEvent event = new B2BCustomerModelCreationEvent(user);

        // publish event to replicate customer after successfully validated
        //eventService.publishEvent(event);

        // replicate B2B customer to Azure AD B2C user account
        replicationService.replicate(user);
    }
}
