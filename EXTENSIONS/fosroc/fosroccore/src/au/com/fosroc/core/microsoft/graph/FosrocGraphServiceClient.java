/*
 * Copyright (c) 2020 DuluxGroup Limited Company. All rights reserved.
 */
package au.com.fosroc.core.microsoft.graph;

import com.microsoft.graph.authentication.IAuthenticationProvider;
import com.microsoft.graph.concurrency.IExecutors;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.core.DefaultClientConfig;
import com.microsoft.graph.core.IClientConfig;
import com.microsoft.graph.http.IHttpProvider;
import com.microsoft.graph.logger.ILogger;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.IUserCollectionRequestBuilder;
import com.microsoft.graph.serializer.ISerializer;

import java.util.Collections;

/**
 * Build a HTTP Client used to send API calls to Microsoft Graph API Endpoint: /users, etc.
 */

public class FosrocGraphServiceClient extends GraphServiceClient {

    private static final String USERS_API_ENDPOINT      = "/users";

    private static final String AUTHENTICATION_PROVIDER = "authenticationProvider";
    private static final String SERIALIZER              = "serializer";
    private static final String HTTP_PROVIDER           = "httpProvider";
    private static final String EXECUTORS               = "executors";
    private static final String LOGGER                  = "logger";

    protected FosrocGraphServiceClient() {}

    @Override
    public IUserCollectionRequestBuilder users() {

        String requestURL = this.getServiceRoot() + USERS_API_ENDPOINT;
        return new FosrocUserCollectionRequestBuilder(requestURL, (IGraphServiceClient) this, Collections.emptyList());
    }

    public static FosrocGraphServiceClient.Builder instance() {

        return new FosrocGraphServiceClient.Builder();
    }

    public static final class Builder {

        public Builder() {}

        public FosrocGraphServiceClient.Initializer authenticationProvider(
                IAuthenticationProvider authenticationProvider) {

            FosrocGraphServiceClient.checkNotNull(authenticationProvider, AUTHENTICATION_PROVIDER);
            return new FosrocGraphServiceClient.Initializer(authenticationProvider);
        }
    }

    public static final class Initializer {

        private final IAuthenticationProvider authenticationProvider;

        private ISerializer serializer;
        private IHttpProvider httpProvider;
        private IExecutors executors;
        private ILogger logger;

        public Initializer(IAuthenticationProvider authenticationProvider) {
            this.authenticationProvider = authenticationProvider;
        }

        public Initializer serializer(ISerializer serializer) {
            checkNotNull(serializer, SERIALIZER);
            this.serializer = serializer;
            return this;
        }

        public Initializer httpProvider(IHttpProvider httpProvider) {
            checkNotNull(httpProvider, HTTP_PROVIDER);
            this.httpProvider = httpProvider;
            return this;
        }

        public Initializer executors(IExecutors executors) {
            checkNotNull(executors, EXECUTORS);
            this.executors = executors;
            return this;
        }

        public Initializer logger(ILogger logger) {
            checkNotNull(logger, LOGGER);
            this.logger = logger;
            return this;
        }

        public IGraphServiceClient buildMSGraphClient() throws ClientException {

            DefaultClientConfig config = new DefaultClientConfig() {

                public IAuthenticationProvider getAuthenticationProvider() {
                    return Initializer.this.authenticationProvider;
                }

                public IHttpProvider getHttpProvider() {
                    return Initializer.this.httpProvider != null ? Initializer.this.httpProvider : super.getHttpProvider();
                }

                public IExecutors getExecutors() {
                    return Initializer.this.executors != null ? Initializer.this.executors : super.getExecutors();
                }

                public ILogger getLogger() {
                    return Initializer.this.logger != null ? Initializer.this.logger : super.getLogger();
                }

                public ISerializer getSerializer() {
                    return Initializer.this.serializer != null ? Initializer.this.serializer : super.getSerializer();
                }
            };

            return FosrocGraphServiceClient.initFromConfig(config);
        }
    }

    public static IGraphServiceClient initFromConfig(IClientConfig config) {

        FosrocGraphServiceClient client = new FosrocGraphServiceClient();

        client.setAuthenticationProvider(config.getAuthenticationProvider());
        client.setExecutors(config.getExecutors());
        client.setHttpProvider(config.getHttpProvider());
        client.setLogger(config.getLogger());
        client.setSerializer(config.getSerializer());
        client.validate();

        return client;
    }

    private static void checkNotNull(Object o, String name) {
        if (o == null) {
            throw new NullPointerException(name + " cannot be null");
        }
    }
}
