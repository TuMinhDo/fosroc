/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.facades.constants;

/**
 * Global class for all FosrocFacades constants.
 */
public class FosrocFacadesConstants extends GeneratedFosrocFacadesConstants
{
	public static final String EXTENSIONNAME = "fosrocfacades";

	private FosrocFacadesConstants()
	{
		//empty
	}
}
