/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.test.constants;

/**
 * 
 */
public class FosrocTestConstants extends GeneratedFosrocTestConstants
{

	public static final String EXTENSIONNAME = "fosroctest";

}
