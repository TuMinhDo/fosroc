/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package au.com.fosroc.initialdata.constants;

/**
 * Global class for all FosrocInitialData constants.
 */
public final class FosrocInitialDataConstants extends GeneratedFosrocInitialDataConstants
{
	public static final String EXTENSIONNAME = "fosrocinitialdata";

	private FosrocInitialDataConstants()
	{
		//empty
	}
}
